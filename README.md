# rpi_docker_mysql_atlassian

#### 介绍
树莓派mysql5.7镜像编译

1. 把项目克隆到树莓派可用目录下
2. build.sh需要有执行权限
3. 如果报错 > unable to prepare context: path ".\r" not found
    可vim 该文件，粘贴`:set fileformat=unix`，然后wq保存即可
4. 运行build.sh执行编译，注意root权限操作
    `sudo bash build.sh`